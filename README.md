# To render

Install dependencies:

    $ sudo dnf install texlive-leaflet redhat-text-fonts redhat-mono-fonts redhat-display-fonts julietaula-montserrat-fonts

Run xelatex:

    $ xelatex centos-leaflet.tex
